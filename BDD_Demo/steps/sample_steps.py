from behave import given, when, then

@given('I have a sample input')
def step_given(context):
    context.sample_input = "Hello, World!"


@when('I perform a sample action')
def step_when(context):
    context.sample_result = perform_sample_action(context.sample_input)


@then('I should get a sample result')
def step_then(context):
    assert context.sample_result == "HELLO, WORLD!"


def perform_sample_action(input_str):
    # Perform some sample action (e.g., convert to uppercase)
    return input_str.upper()
